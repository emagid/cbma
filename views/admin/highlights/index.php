<?php
if(count($model->highlights) > 0) { ?>
	<div class="box box-table">
		<table class="table">
			<thead>
				<tr>
					<th width=5%>Id</th>
					<th width=20%>Image</th>
					<th width="10%" class="text-center">Edit</th>
          			<th width="10%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model->highlights as $obj){  ?>
					<tr>
						<td><a href="<?php echo ADMIN_URL; ?>highlights/update/<?php echo $obj->id?>"><?php echo $obj->id;?></a></td>
						<td><a href="<?php echo ADMIN_URL;?>highlights/update/<?php echo $obj->id; ?>"><img src="/content/uploads/highlights/<?php echo $obj->image; ?>" style="max-width: 125px; max-height: 100px; "></a></td>
						<td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>highlights/update/	<?= $obj->id ?>">
				           		<i class="icon-pencil"></i> 
				           	</a>
				        </td>
				        <td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>highlights/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
				           		<i class="icon-cancel-circled"></i> 
				           	</a>
				        </td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
<? } else { ?>
	<h4>There are no highlight banners yet. </h4>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'highlights';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>