<div id='kiosk3' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk3"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<img id='close' src="<?= FRONT_ASSETS ?>img/x_dark.png" style='color: white;'>
<section class='slider'>
	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk3_img2.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>RUMBLE YOUNG MAN, RUMBLE (RYMR)</h4>
		</div>
		<div class='quotes'>
			<h4>RUMBLE YOUNG MAN, RUMBLE (RYMR)</h4>
			<p>Since 2011, CBMA's signature event “Rumble Young Man Rumble!” has convened leaders nationwide to share promising practices and lessons learned, and to create collaborations with Black men and boys in communities across the country. Due to the success of the annual Rumble convening held each year in Louisville, Kentucky, CBMA has organized regional Rumbles in Detroit, Baltimore and Milwaukee. In the summer of 2018, CBMA launched the “Flyweight Training Camp,”  a pilot program to develop 50 young Black men and boys to accelerate their leadership skills, personal development, and academic performance, enhance their social interactions, and increase their cultural awareness. The camp included a ten-day international study-abroad trip to Belize.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk3_img3.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>MLK NOW</h4>
		</div>
		<div class='quotes'>
			<h4>MLK NOW</h4>
			<p>Since 2016, the Campaign for Black Male Achievement has joined Blackout for Human Rights to host "MLK Now" a special program of speech and musical performances honoring the words and the legacy of Dr. Martin Luther King, Jr., along with other iconic human and civil rights leaders. Held at the historic Riverside Church in New York City, MLK Now has featured performers such as Ryan Coogler, Harry Belafonte, Octavia Spencer, Chris Rock, Lupita Nyong’o, Michael B. Jordan, J. Cole, Jussie Smollett, India.Arie, Andre Holland, Yara Shahidi, and more.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk3_img4.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>PROMISE OF PLACE NATIONAL CONVENING</h4>
		</div>
		<div class='quotes'>
			<h4>PROMISE OF PLACE NATIONAL CONVENING</h4>
			<p>In October 2017, CBMA hosted the first Promise of Place (PoP) City Convening in Louisville, KY, bringing together about 50 national leaders from 10 cities representing city government, philanthropy, community organizations, and school districts to share best practices and build upon our collective work and aspirational goals for Black men and boys. The 2018 PoP Convening will take place on November 11th and 12th at the Muhammad Ali Center in Louisville.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk3_img5.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>CBMA | FUND II FOUNDATION RESTORATION RETREAT</h4>
		</div>
		<div class='quotes'>
			<h4>CBMA | FUND II FOUNDATION RESTORATION RETREAT</h4>
			<p>In July 2018, the Campaign for Black Male Achievement joined the Fund II Foundation to host the inaugural Restoration Retreat in Lincoln Hills, CO on the beautiful, sprawling property of the Foundation's founder and visionary philanthropist, Robert F. Smith. At the retreat, CBMA convened 35 young Black men from our five Promise of Place cities -- Detroit, Louisville, Baltimore, Milwaukee and Oakland -- for an immersive and transformative experience that included yoga, meditation, leadership development, and co-existing with nature. </p>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 3000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
			$(this).removeClass('open');
			$('.info h4').fadeIn();
			$('#close').fadeOut();
			
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x_dark.png');
			$(this).addClass('open');
			$('.info h4').fadeOut();
			$('#close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
	});
</script>