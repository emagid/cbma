<div id='kiosk2' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk2"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text_full'>
	<p>CBMA is excited to host our 10th Anniversary “Black Male Re-imagined” Gala and Fundraiser to celebrate a decade of movement and field building to uplift Black men and boys! Learn more about our honorees George Soros, Tonya Allen and John W. Rogers, Jr. and our host Hill Harper.</p>
</section>
<h3 class='ten'>10</h3>

<button class='btn'>LEARN MORE<i class="fas fa-caret-right"></i></button>

<img id='close' src="<?= FRONT_ASSETS ?>img/x_dark.png" style='color: white;'>
<section class='slider'>
	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk2_img2.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>BMA Health and Healing Strategies (HHS) Initiative</h4>
		</div>
		<div class='quotes'>
			<h4>BMA Health and Healing Strategies (HHS) Initiative</h4>
			<p>CBMA’s Health and Healing Strategies initiative aims to improve the health outcomes of Black males by promoting self-empowerment and wellness education among leaders in Black Male Achievement (BMA). Launched in 2016, these Health and Healing Strategies (HHS) are designed to ensure that leaders in the BMA field have the tools and resources to facilitate and sustain their health and healing, and that of the Black males and broader communities that they serve. BMA HHS has partnered with community groups as well as schools to develop and implement programming that seeks to reduce the effects of trauma and improve the health and viability of our communities. More than 30 BMA HHS workshops have been conducted, reaching 772 community leaders, parents, guardians, mentors, and school personnel from Oakland and Sacramento.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk2_img6.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>American Express Leadership Academy at CBMA</h4>
		</div>
		<div class='quotes'>
			<h4>American Express Leadership Academy at CBMA</h4>
			<p>With support from the American Express Foundation, CBMA has designed and implemented a customized leadership development experience for emerging leaders in the Black Male Achievement field. The American Express Leadership Academy at CBMA utilizes the unique strengths and competencies of two Greensboro, North Carolina-based organizations -- the Center for Creative Leadership (CCL) and the Beloved Community Center (BCC) -- to engage leaders in a year-long learning community to help ensure individual effectiveness and impact in organizational leadership within the broader field of Black Male Achievement.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk2_img4.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>BLACK MALE RE-IMAGINED</h4>
		</div>
		<div class='quotes'>
			<h4>BLACK MALE RE-IMAGINED</h4>
			<p>Black Male Re-Imagined is part of CBMA’s ongoing narrative change work to ensure more accurate depictions about Black men and boys and their communities through a series of public events, social media campaigns and partnerships with mainstream outlets such as Vice. com, Cassius, and the Root. The concept for Black Male: Re-Imagined was formed in partnership with the Perception Institute and CBMA to create a safe and honest space of conversation and action focused on real solutions to help drive a more accurate portrayal of Black men and boys in mainstream media and news. Three events have been held to date: In December 2010 and April 2013 in partnership with the Knight and Ford Foundations; and in October 2016 at the Kennedy Center in Washington, D.C.</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk2_img5.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>BLACK MALE ACHIEVEMENT LEADERS IN RESIDENCE PROGRAM</h4>
		</div>
		<div class='quotes'>
			<h4>BLACK MALE ACHIEVEMENT LEADERS IN RESIDENCE PROGRAM</h4>
			<p>CBMA’s Black Male Achievement Leaders in Residence Program, in partnership with the University of Louisville and Metro United Way with funding support from Kenan Charitable Trust, is a customized 12-month leadership development experience for a cohort of 10 senior professionals in the BMA field. Fellows participate in six intensive sessions—two in person, four online—designed to strengthen skills in areas such as organizational development, succession planning, resource development, strategic communications and public policy. </p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk2_img3.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>BLACK MALE EQUITY INITIATIVE (BMEI)</h4>
		</div>
		<div class='quotes'>
			<h4>BLACK MALE EQUITY INITIATIVE (BMEI)</h4>
			<p>CBMA, in partnership with Dr. Pamela Jolly, CEO of Torch Enterprises, created BMEI in Detroit in 2018 to expand the perspective of Black male achievement to include equity and ownership as keys to long-term wealth creation. BMEI engages Detroit Black male leaders aged 18-75 in a 12-month exploration of financial equity with the goal of establishing an individual definition of wealth creation and a path towards it. </p>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 3000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
			$(this).removeClass('open');
			$('.info h4').fadeIn();
			$('#close').fadeOut();
			
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x_dark.png');
			$(this).addClass('open');
			$('.info h4').fadeOut();
			$('#close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
	});
</script>