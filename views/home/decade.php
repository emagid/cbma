<div id='kiosk5' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk5"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text'>
	<p>What will it take to further advance life outcomes for Black men and boys over the next 10 years? Share your thoughts on social media using the hashtags <strong>#CBMATurns10</strong> and <strong>#LoveLearnLead!</strong> </p>
	<h2>#CBMATurns10</h2>
	<h2>#LoveLearnLead</h2>
</section>

<section class='background_text' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk5_img3.jpg');">
</section>