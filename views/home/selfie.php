<div id='kiosk3' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk3"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text'>
	<p>Which of the below core principles championed throughout Muhammad Ali’s life and career resonates with you most? Take a photo, then pick the frame that best captures your choice!</p>
	<div class='action cam'>
		<i class="fas fa-camera"></i>
	</div>
</section>

<section class='background_text' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk3_img6.jpg');">
	<div class='content list'>
		<h2>Confidence</h2>
		<h2>Conviction</h2>
		<h2>Dedication</h2>
		<h2>Giving</h2>
		<h2>Respect</h2>
		<h2>Spirituality</h2>
	</div>
</section>

<script type="text/javascript">
	$('.cam').click(function(){
		$('.white').fadeIn();
		setTimeout(function(){
			window.location = "/home/photobooth2"
		}, 500);
	});
</script>