<div id='kiosk2' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk2"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder gold'>
	<h2>CBMA: A DECADE OF INVESTMENT, INNOVATION & IMPACT</h2>
</section>

<section class='text_full'>
	<p>With the collective leadership of the field of Black Male Achievement, CBMA has:</p>
	<ul>
		<li><span>Catalyzed more than <span class='large'>$320 million</span> in national and local funds for Black Male Achievement</span></li>
		<li><span>Built a vibrant BMA network of <span class='large'>5,700+ members</span> and <span class='large'>2,765+ organizations</span> (and growing)</span></li>
		<li><span>Seeded several national initiatives, including My Brother’s Keeper, to establish a growing field</span></li>
		<li><span>Aided launch of local initiatives focused on equity and Black Male Achievement in cities across the U.S. </span></li>
		<li><span>Broadened the BMA field to include healing, narrative change and Black women and girls</span></li>
		<li><span>Nearly two-thirds of CBMA network survey respondents reported having strengthened their own leaderships skills or the leadership skills of their colleagues from being part of our network.</span></li>
	</ul>
</p>
</section>

