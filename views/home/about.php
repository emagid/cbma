<div id='kiosk1' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk1"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text_full'>
	<h2 class='title'>About the Campaign for Black Male Achievement (CBMA)</h2>
	<p>CBMA is a national membership network that seeks to ensure the growth, sustainability, and impact of leaders and organizations committed to improving the life outcomes of Black men and boys. Since 2008, CBMA has charted a courageous way forward to support leaders on the ground while amplifying and catalyzing Black Male Achievement around the country.</p>
</section>
<h3 class='ten'>10</h3>